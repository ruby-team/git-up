Source: git-up
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: James Lu <bitflip3@gmail.com>
Build-Depends: debhelper (>= 9),
               gem2deb,
               ruby-colored (>= 1.2),
               ruby-grit
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/git-up.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/git-up.git
Homepage: http://aanandprasad.com/git-up/
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: git-up
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: git,
         ruby | ruby-interpreter,
         ruby-colored (>= 1.2),
         ruby-grit,
         ${misc:Depends},
         ${shlibs:Depends}
Description: fetch and rebase all locally-tracked remote Git branches
 git-up solves the following 'git pull' problems:
 .
 * It merges upstream changes by default, when it's really more polite to
 rebase over them, unless your collaborators enjoy a commit graph that looks
 like bedhead.
 * It only updates the branch you're currently on, which means git push will
 shout at you for being behind on branches you don't particularly care about
 right now.
